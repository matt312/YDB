FROM alpine:edge  as alpine-locale 


ENV MUSL_LOCALE_DEPS cmake make musl-dev gcc gettext-dev libintl samurai
ENV MUSL_LOCPATH /usr/share/i18n/locales/musl

RUN apk add --no-cache \
    $MUSL_LOCALE_DEPS

RUN apk add --no-cache \
    $MUSL_LOCALE_DEPS \
    && wget https://gitlab.com/rilian-la-te/musl-locales/-/archive/master/musl-locales-master.zip \
    && unzip musl-locales-master.zip \
      && cd musl-locales-master \
      && cmake -DLOCALE_PROFILE=OFF -D CMAKE_INSTALL_PREFIX:PATH=/usr -G Ninja . && ninja && ninja install \
      && cd .. && rm -r musl-locales-master



FROM alpine-locale as ydb-release-builder

RUN apk add file build-base cmake tcsh libconfig-dev elfutils-dev libgcrypt-dev libgpg-error-dev gpgme-dev ncurses-dev icu-dev openssl-dev zlib-dev binutils-gold libexecinfo-dev samurai musl-dev linux-headers


CMD mkdir -p /tmp/yottadb-build \
 && cd /tmp/yottadb-build \
 && test -f /tmp/yottadb-src/.yottadb.vsn || \
    grep YDB_ZYRELEASE /tmp/yottadb-src/sr_*/release_name.h \
    | grep -o '\(r[0-9.]*\)' \
    | sort -u \
    > /tmp/yottadb-src/.yottadb.vsn \
 && cmake \
      -D CMAKE_INSTALL_PREFIX:PATH=/tmp \
      -D YDB_INSTALL_DIR:STRING=yottadb-release \
      -D CMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE} \
      /tmp/yottadb-src \
      -G Ninja \
 && ninja \
 && ninja install



