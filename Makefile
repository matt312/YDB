.PHONY: all debian 

DOCKER_BUILDKIT:=1
export DOCKER_BUILDKIT

BUILDER ?= docker 
BUILD_CMD ?= build --progress=plain

TZ:= $(shell readlink /etc/localtime )

all:

alpine:
	$(BUILDER) $(BUILD_CMD) -t yottadb:alpine -f alpine.Dockerfile .

buildah:
	BUILDER=buildah BUILD_CMD=build-using-dockerfile make alpine
podman:
	podman pull docker.io/library/alpine:edge
	BUILDER="podman  --cgroup-manager=cgroupfs" make alpine

run:
	$(BUILDER) run --rm -it --name YottaDBAlpine --hostname yottalpine -e TZ=$(TZ) yottadb:alpine

run-podman:
	BUILDER="podman" make run
