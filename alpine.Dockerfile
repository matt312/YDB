FROM alpine:edge  as alpine-locale 


ENV MUSL_LOCALE_DEPS cmake make musl-dev gcc gettext-dev libintl samurai
ENV MUSL_LOCPATH /usr/share/i18n/locales/musl

RUN apk add --no-cache \
    $MUSL_LOCALE_DEPS

RUN apk add --no-cache \
    $MUSL_LOCALE_DEPS \
    && wget https://gitlab.com/rilian-la-te/musl-locales/-/archive/master/musl-locales-master.zip \
    && unzip musl-locales-master.zip \
      && cd musl-locales-master \
      && cmake -DLOCALE_PROFILE=OFF -D CMAKE_INSTALL_PREFIX:PATH=/usr -G Ninja . && ninja && ninja install \
      && cd .. && rm -r musl-locales-master



FROM alpine-locale as ydb-release-builder

RUN apk add file build-base cmake tcsh libconfig-dev elfutils-dev libgcrypt-dev libgpg-error-dev gpgme-dev ncurses-dev icu-dev openssl-dev zlib-dev binutils-gold libexecinfo-dev samurai musl-dev linux-headers

RUN mkdir -p /tmp/yottadb-src
COPY cmake /tmp/yottadb-src/cmake
COPY sr_aarch64/ /tmp/yottadb-src/sr_aarch64
COPY sr_armv7l/  /tmp/yottadb-src/sr_armv7l
COPY sr_i386/    /tmp/yottadb-src/sr_i386
COPY sr_linux/   /tmp/yottadb-src/sr_linux
COPY sr_port/    /tmp/yottadb-src/sr_port
COPY sr_port_cm/ /tmp/yottadb-src/sr_port_cm
COPY sr_unix/    /tmp/yottadb-src/sr_unix
COPY sr_unix_cm/ /tmp/yottadb-src/sr_unix_cm
COPY sr_unix_gnp/ /tmp/yottadb-src/sr_unix_gnp
COPY sr_unix_nsb/ /tmp/yottadb-src/sr_unix_nsb
COPY sr_x86_64/   /tmp/yottadb-src/sr_x86_64
COPY sr_x86_regs/ /tmp/yottadb-src/sr_x86_regs
COPY CMakeLists.txt /tmp/yottadb-src/

RUN ls -alh /tmp/yottadb-src/

RUN mkdir -p /tmp/yottadb-build \
 && cd /tmp/yottadb-build \
 && test -f /tmp/yottadb-src/.yottadb.vsn || \
    grep YDB_ZYRELEASE /tmp/yottadb-src/sr_*/release_name.h \
    | grep -o '\(r[0-9.]*\)' \
    | sort -u \
    > /tmp/yottadb-src/.yottadb.vsn \
 && cmake \
      -D CMAKE_INSTALL_PREFIX:PATH=/tmp \
      -D YDB_INSTALL_DIR:STRING=yottadb-release \
      -D CMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE} \
      /tmp/yottadb-src \
      -G Ninja \
 && ninja \
 && ninja install



FROM alpine-locale 
RUN apk add --no-cache pkgconf bash file icu-dev wget libelf

WORKDIR /data
ENV gtmdir=/data \
    LANG=en_US.UTF-8 \
    LANGUAGE=en_US:en \
    LC_ALL=en_US.UTF-8


ENV PATH=$PATH:/opt/yottadb/current
ENV ydb_dir=/data 
ENV ydb_dist=/opt/yottadb/current
ENV ydb_gbldir=/data/yottadb/g/yottadb.gld

RUN mkdir -p /data/yottadb; \
mkdir -p /data/yottadb/g; \
mkdir -p /data/yottadb/r; \
mkdir -p /data/yottadb/o; 

COPY --from=ydb-release-builder /tmp/yottadb-release /tmp/yottadb-release
WORKDIR /tmp/yottadb-release
SHELL ["/bin/bash", "-c"]
RUN pkg-config --modversion icu-io > /tmp/yottadb-release/.icu.vsn
RUN icu=$(</tmp/yottadb-release/.icu.vsn); export icu; echo $icu; \
mkdir -p /usr/share/pkgconfig && \
./ydbinstall \
      --utf8 $icu \
      --installdir /opt/yottadb/current \
      --force-install \
 && rm -rf /tmp/yottadb-release
WORKDIR /data
